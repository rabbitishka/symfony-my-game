<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'minlength' => '2',
                    'maxlength' => '50',
                    'autocomplete' => 'username'
                ],
                    'label' => 'Pseudo',
                    'label_attr' => [
                        'class' => 'form-label mt-4'
                ],
                    'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez votre Pseudo.',
                    ]),
                    new Length([
                        'min' => '2',
                        'minMessage' => 'Votre pseudo doit avoir au minimum {{ limit }}',
                        'max' => '50',
                        'maxMessage' => 'Votre pseudonyme ne doit pas dépasser {{ limit }}'
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'minlength' => '5',
                    'maxlength' => '180',
                    'autocomplete' => 'email'
                ],
                    'label' => 'Email',
                    'label_attr' => [
                        'class' => 'form-label mt-4'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez votre email.',
                    ]),
                    // new Length([
                    //     'min' => '5',
                    //     'minMessage' => 'Votre email doit avoir au minimum {{ limit }}',
                    //     'max' => '180',
                    //     'maxMessage' => 'Votre email ne doit pas dépasser {{ limit }}'
                    // ])
                ] 
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez votre mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit avoir au minimum {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
