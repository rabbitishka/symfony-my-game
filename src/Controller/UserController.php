<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Form\UserType;

class UserController extends AbstractController
{
    #[Route('/mon_espace', name: 'app_user')]
    public function index(): Response
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    // #[Route('/mon_espace', name: 'app_user')]
    // // public function index(User $user): Response
    // public function index(): Response
    // {
    //     // if(!$this->getUser()) {
    //     //     return $this->redirectToRoute('app_login');
    //     // }

    //     // if($this->getUser() !== $user) {
    //     //     return $this->redirectToRoute('app_home');
    //     // }

    //     // $form = $this->createForm(UserType::class, $user);

    //     return $this->render('app_user');
    //     // , [
    //     //     'form' => $form->createView(),
    //     // ]);
    // }
}
