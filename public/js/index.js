
// Define the variables
let clickMooncake = document.querySelector("#mooncake"),
    updateScore = document.querySelector("#counter"),
    timerWhileClick = document.querySelector("#timer"),
    timerBeforeStart = document.querySelector("#timer2"),
    counterClicks = 0,
    countdownWhileClick = 10,
    countdownBeforeStart = 5

// Function to count clicks
function allClicks() {
    counterClicks++;
    updateScore.textContent = counterClicks;
    let result = counterClicks
}

// Timer to make clicks while gaming
setTimeout(() => {
    let intervalWhileClick = setInterval(() => {
        countdownWhileClick--
        timerWhileClick.textContent = countdownWhileClick
        if (countdownWhileClick < 1) {
            clearInterval(intervalWhileClick)
            timerWhileClick.textContent = 'BASTA !'
            clickMooncake.removeEventListener('click', allClicks)
        }
    }, 1000);
}, 5000);

// Timer before starting the game
let intervalBeforeStart = setInterval(() => {
    countdownBeforeStart--
    timerBeforeStart.textContent = countdownBeforeStart
    if (countdownBeforeStart < 1) {
        clearInterval(intervalBeforeStart)
        timerBeforeStart.textContent = 'ALLEZ GO !'
        clickMooncake.addEventListener('click', allClicks)
    }
}, 1000);